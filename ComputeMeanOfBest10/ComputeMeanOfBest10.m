clear;clc

%% User Control
%DirAddr = 'C:\Datasets\Gun_Point\Inside';
%CoralFileAddr = [DirAddr '\' 'Gun_Point_OutputIQ_Bsf_ST30.csv'];
%JacorFileAddr = [DirAddr '\' '\Gun_Point_JACOR_Outputfile.csv'];
%JacorLAFileAddr = [DirAddr '\' '\Gun_Point_JACOR-LA_Outputfile.csv'];
%PAAFileAddr = [DirAddr '\' '\Gun_Point_PAA_IQ.csv'];

%DirAddr = 'C:\Datasets\Gun_Point\Outside';
%CoralFileAddr = [DirAddr '\' 'Gun_Point_OutputOQ_Bsf_ST30.csv'];
%JacorFileAddr = [DirAddr '\' '\Gun_Point_JACOR_Outputfile.csv'];
%JacorLAFileAddr = [DirAddr '\' '\Gun_Point_JACOR-LA_Outputfile.csv'];
%PAAFileAddr = [DirAddr '\' '\Gun_Point_PAA_OQ.csv'];

%DirAddr = 'C:\Datasets\MedicalImages\Inside';
%CoralFileAddr = [DirAddr '\' 'MedicalImages_OutputIQ_Bsf_ST30.csv'];
%JacorFileAddr = [DirAddr '\' '\MedicalImages_JACOR_Outputfile.csv'];
%JacorLAFileAddr = [DirAddr '\' '\MedicalImages_JACOR-LA_Outputfile.csv'];
%PAAFileAddr = [DirAddr '\' '\MedicalImages_PAA_IQ.csv'];

DirAddr = 'C:\Datasets\MedicalImages\Outside';
CoralFileAddr = [DirAddr '\' 'MedicalImages_OutputOQ_Bsf_ST30.csv'];
JacorFileAddr = [DirAddr '\' '\MedicalImages_JACOR_Outputfile.csv'];
JacorLAFileAddr = [DirAddr '\' '\MedicalImages_JACOR-LA_Outputfile.csv'];
PAAFileAddr = [DirAddr '\' '\MedicalImages_PAA_OQ.csv'];


OutputFile = 'C:\CORAL_System\MATLABWorkspace\ComputeMeanOfBest10\temp.csv'



CoralErrIdx = 10;
CoralTimeIdx = 9;

NaiveCorrIdx = 15;
NaiveTimeIdx = 16;

JacorCorrIdx = 5;
JacorTimeIdx = [6 7 8];

PaaCorrIdx = 5;
PaaTimeIdx = 6;


%% Initialiation
CoralData = csvread(CoralFileAddr);
CoralData = CoralData(2:21,1:16);

JacorData = csvread(JacorFileAddr);
JacorData = JacorData(2:21,:);

JacorLAData = csvread(JacorLAFileAddr);
JacorLAData  = JacorLAData (2:21,:);

PaaData = csvread(PAAFileAddr);
PaaData = PaaData(2:21,:);


%% Algorithm
CoralError = CoralData(:,CoralErrIdx);

SortedCoralError =sort(CoralError);
Threshold = SortedCoralError(10);
Mask = CoralError<=Threshold;


CoralMeanError = mean(CoralData(Mask,CoralErrIdx));
CoralAccuracy = (1-CoralMeanError)*100;
CoralMeanTime = mean(CoralData(Mask,CoralTimeIdx));

NaiveCorr = abs(CoralData(Mask,NaiveCorrIdx));
NaiveMeanTime = mean(CoralData(Mask,NaiveTimeIdx));


JacorCorr = abs(JacorData(Mask,JacorCorrIdx));
JacorTime = JacorData(Mask,JacorTimeIdx);
JacorAccuracy = 100*(1-mean(abs(JacorCorr-NaiveCorr)));
JacorMeanTime = mean(JacorTime);

JacorLACorr = abs(JacorLAData(Mask,JacorCorrIdx));
JacorLATime = JacorLAData(Mask,JacorTimeIdx);
JacorLAAccuracy = 100*(1-mean(abs(JacorLACorr-NaiveCorr)));
JacorLAMeanTime = mean(JacorLATime);


PaaCorr = abs(PaaData(Mask,PaaCorrIdx));
PaaTime = PaaData(Mask,PaaTimeIdx);
Paaccuracy = 100*(1-mean(abs(PaaCorr-NaiveCorr)));
PaaMeanTime = mean(PaaTime);

%% Output
OutData = [CoralAccuracy CoralMeanTime NaiveMeanTime Paaccuracy PaaMeanTime JacorAccuracy JacorMeanTime JacorLAAccuracy JacorLAMeanTime];

csvwrite(OutputFile,OutData);







