clear;clc;

%% User Control
DatasetListFileAddr = 'C:\Datasets\FileList-SizeSorted2-1.xlsx';
DatasetListFileRange = 'A2:H86';

OutputFileDir = 'C:\Datasets\Run2';

TopN = 50;

AccDataFileName = 'AccuracyData_Top50_Run2-22.mat';
TimeDataFileName = 'TimeData_Top50_Run2-22.mat';

%% Initialization
[Data,DatasetNameList] = xlsread(DatasetListFileAddr,DatasetListFileRange);
LVec = Data(:,2);
NVec = Data(:,1);               % This is N for original data
N = length(DatasetNameList);        % no. of datasets

MeanAccuracyCORAL = zeros();


% Column 
M = containers.Map();
M('CC') = 8;            % CORAL correlation
M('CT') = 9;            % CORAL time
M('CE') = 10;           % CORAL Error
M('NC') = 15;           % NAIVE correlation
M('NT') = 16;           % NAIVE time
M('PC') = 5;            % PAA correlation
M('PT') = 6;            % PAA time
M('JC') = 5;            % JACOR correlation
M('JT') = 6;            % JACOR time
M('JLC') = 5;           % JACOR-LA correlation
M('JLT') = 6;           % JACOR-LA time
M('JFT') = 7;           % JACOR FFT time



NAIVE_Time = zeros(N,1);
CORAL_Time = zeros(N,1);
PAA_Time = zeros(N,1);
JACOR_Time = zeros(N,1);
JACOR_LA_Time = zeros(N,1);
JACOR_SearchTime = zeros(N,1);
JACOR_LA_SearchTime = zeros(N,1);


CORAL_Acc = zeros(N,1);
PAA_Acc = zeros(N,1);
JACOR_Acc = zeros(N,1);
JACOR_LA_Acc = zeros(N,1);


%% Algorithm

for k = 1:N
    
	% Prepare Dataset names
	DatasetName = DatasetNameList{k};                   % Dataset name with extension
    DatasetNameOnly = DatasetNameList{k}(1:end-4);      % Dataset name without extension
    DatasetExt = DatasetNameList{k}(end-2:end);
	
	% Load data from output files
    NAIVE_Data = LoadData(OutputFileDir,DatasetNameOnly,"NAIVE");
	CORAL_Data = LoadData(OutputFileDir,DatasetNameOnly,"CORAL");
	PAA_Data = LoadData(OutputFileDir,DatasetNameOnly,"PAA");
	JACOR_Data = LoadData(OutputFileDir,DatasetNameOnly,"JACOR");
	JACOR_LA_Data = LoadData(OutputFileDir,DatasetNameOnly,"JACOR-LA");
	
	% Find top N queries from CORAL
%     [~,idx1] = sort(CORAL_Data{1}(:,M('CE')));        % for inside queries
%     [~,idx2] = sort(CORAL_Data{2}(:,M('CE')));        % for outside queries
%     
%     S1 = idx1(1:TopN);
%     S2 = idx2(1:TopN);
    [S1,S2] = GetTopNQuerySamples(CORAL_Data,NAIVE_Data,M('CC'),M('NC'),TopN);



	% Select corresponding query data
    NAIVE_Data{1} = NAIVE_Data{1}(S1,:);
    NAIVE_Data{2} = NAIVE_Data{2}(S2,:);
    CORAL_Data{1} = CORAL_Data{1}(S1,:);
    CORAL_Data{2} = CORAL_Data{2}(S2,:);
    PAA_Data{1} = PAA_Data{1}(S1,:);
    PAA_Data{2} = PAA_Data{2}(S2,:);
    JACOR_Data{1} = JACOR_Data{1}(S1,:);
    JACOR_Data{2} = JACOR_Data{2}(S2,:);
    JACOR_LA_Data{1} = JACOR_LA_Data{1}(S1,:);
    JACOR_LA_Data{2} = JACOR_LA_Data{2}(S2,:);
    
    
	% Compute mean time
    NAIVE_Time(k) = ComputeMeanTime(NAIVE_Data, M('NT'));
    CORAL_Time(k) = ComputeMeanTime(CORAL_Data, M('CT'));
    PAA_Time(k) = ComputeMeanTime(PAA_Data, M('PT'));
    JACOR_Time(k) = ComputeMeanTime(JACOR_Data, M('JT'));
    JACOR_LA_Time(k) = ComputeMeanTime(JACOR_LA_Data, M('JLT'));
    
    JACOR_SearchTime(k) = GetJacorMeanSearchTime(JACOR_Data,M('JT'),M('JFT'));
    JACOR_LA_SearchTime(k) = GetJacorMeanSearchTime(JACOR_LA_Data,M('JLT'),M('JFT'));

	% Compute accuracy
    CORAL_Acc(k) = ComputeAccuracy(CORAL_Data,NAIVE_Data,M('CC'),M('NC'));
    PAA_Acc(k) = ComputeAccuracy(PAA_Data,NAIVE_Data,M('PC'),M('NC'));
    JACOR_Acc(k) = ComputeAccuracy(JACOR_Data,NAIVE_Data,M('JC'),M('NC'));
    JACOR_LA_Acc(k) = ComputeAccuracy(JACOR_LA_Data,NAIVE_Data,M('JLC'),M('NC'));
    
    
    
    
    fprintf('%d. Processed: %s\n',k,DatasetNameOnly);
    
end



%% Results
% 
save(TimeDataFileName, '*Time');
save(AccDataFileName,'*_Acc');









