% Goal of this script is to copy the top50 queries for sharing
clear;clc;

%% User Control
DatasetListFileAddr = 'C:\Datasets\FileList-SizeSorted2-1.xlsx';
DatasetListFileRange = 'A2:H86';

OutputFileDir = 'C:\Datasets\Run2';         % output directory where results are written
QueryCopyDir = 'C:\Datasets\Top50Query';               % directory where queries are to be copied

TopN = 50;

%% Initialization
[Data,DatasetNameList] = xlsread(DatasetListFileAddr,DatasetListFileRange);
LVec = Data(:,2);
NVec = Data(:,1);               % This is N for original data
N = length(DatasetNameList);        % no. of datasets

MeanAccuracyCORAL = zeros();


% Column 
M = containers.Map();
M('CC') = 8;            % CORAL correlation
M('CT') = 9;            % CORAL time
M('CE') = 10;           % CORAL Error
M('NC') = 15;           % NAIVE correlation
M('NT') = 16;           % NAIVE time
M('PC') = 5;            % PAA correlation
M('PT') = 6;            % PAA time
M('JC') = 5;            % JACOR correlation
M('JT') = 6;            % JACOR time
M('JLC') = 5;           % JACOR-LA correlation
M('JLT') = 6;           % JACOR-LA time
M('JFT') = 7;           % JACOR FFT time


CORAL_Acc = zeros(N,1);
PAA_Acc = zeros(N,1);
JACOR_Acc = zeros(N,1);
JACOR_LA_Acc = zeros(N,1);

%% Algorithm

for k=1:N
    
	% Prepare Dataset names
	DatasetName = DatasetNameList{k};                   % Dataset name with extension
    DatasetNameOnly = DatasetNameList{k}(1:end-4);      % Dataset name without extension
    DatasetExt = DatasetNameList{k}(end-2:end);
% 	
% 	% Load data from output files
%     NAIVE_Data = LoadData(OutputFileDir,DatasetNameOnly,"NAIVE");
% 	CORAL_Data = LoadData(OutputFileDir,DatasetNameOnly,"CORAL");
% 	PAA_Data = LoadData(OutputFileDir,DatasetNameOnly,"PAA");
% 	JACOR_Data = LoadData(OutputFileDir,DatasetNameOnly,"JACOR");
% 	JACOR_LA_Data = LoadData(OutputFileDir,DatasetNameOnly,"JACOR-LA");
%    
%     [S1,S2] = GetTopNQuerySamples(CORAL_Data,NAIVE_Data,M('CC'),M('NC'),TopN);
%     
%     S1 = sort(S1);
%     S2 = sort(S2);
    
%     % Copy each file to new location with new name
%     mkdir(sprintf('%s\\%s\\Inside',QueryCopyDir,DatasetNameOnly));
%     mkdir(sprintf('%s\\%s\\Outside',QueryCopyDir,DatasetNameOnly));
%      
%       % for inside queries
%     for i=1:length(S1)
%         Source = sprintf('%s\\%s\\Inside\\InsideQuery%d.txt',OutputFileDir,DatasetNameOnly,S1(i)-1);
%         Dest =  sprintf('%s\\%s\\Inside\\InsideQuery%d.txt',QueryCopyDir,DatasetNameOnly,i-1);
%         system(sprintf('copy %s %s',Source,Dest));
%     end
%       
%       % for outside queries
%       for i=1:length(S2)       
%         Source = sprintf('%s\\%s\\Outside\\OutsideQuery%d.txt',OutputFileDir,DatasetNameOnly,S2(i));
%         Dest =  sprintf('%s\\%s\\Outside\\OutsideQuery%d.txt',QueryCopyDir,DatasetNameOnly,i-1);
%         system(sprintf('copy %s %s',Source,Dest));   
%       end
%     
% 
%     
%     % Update the data files
%     NAIVE_Data{1} = NAIVE_Data{1}(S1,:);            NAIVE_Data{2} = NAIVE_Data{2}(S2,:);
%     CORAL_Data{1} = CORAL_Data{1}(S1,:);            CORAL_Data{2} = CORAL_Data{2}(S2,:);
%     PAA_Data{1} = PAA_Data{1}(S1,:);                PAA_Data{2} = PAA_Data{2}(S2,:);
%     JACOR_Data{1} = JACOR_Data{1}(S1,:);            JACOR_Data{2} = JACOR_Data{2}(S2,:);
%     JACOR_LA_Data{1} = JACOR_LA_Data{1}(S1,:);      JACOR_LA_Data{2} = JACOR_LA_Data{2}(S2,:);
%     
%     % update the query indexes
%     v1 = (0:length(S1)-1)';
%     v2 = (0:length(S2)-1)';
%     
%     NAIVE_Data{1}(:,1) = v1;     NAIVE_Data{2}(:,1) = v2;
%     CORAL_Data{1}(:,1) = v1;     CORAL_Data{2}(:,1) = v2;
%     PAA_Data{1}(:,1) = v1;       PAA_Data{2}(:,1) = v2;
%     JACOR_Data{1}(:,1) = v1;     JACOR_Data{2}(:,1) = v2;
%     JACOR_LA_Data{1}(:,1) = v1;  JACOR_LA_Data{2}(:,1) = v2;
% 
%     % Write corresponding csv file
%     CORAL_File = sprintf('%s\\%s\\Inside\\%s_CORAL_IQ.csv',QueryCopyDir,DatasetNameOnly,DatasetNameOnly);
%     PAA_File = sprintf('%s\\%s\\Inside\\%s_PAA_IQ.csv',QueryCopyDir,DatasetNameOnly,DatasetNameOnly);
%     JACOR_File = sprintf('%s\\%s\\Inside\\%s_JACOR_IQ.csv',QueryCopyDir,DatasetNameOnly,DatasetNameOnly);
%     JACOR_LA_File = sprintf('%s\\%s\\Inside\\%s_JACOR-LA_IQ.csv',QueryCopyDir,DatasetNameOnly,DatasetNameOnly);
%     
%     dlmwrite(CORAL_File,CORAL_Data{1},'delimiter',',','precision',4);
%     dlmwrite(PAA_File,PAA_Data{1},'delimiter',',','precision',4);
%     dlmwrite(JACOR_File,JACOR_Data{1},'delimiter',',','precision',4);
%     dlmwrite(JACOR_LA_File,JACOR_LA_Data{1},'delimiter',',','precision',4);
%     
%     
%     CORAL_File = sprintf('%s\\%s\\Outside\\%s_CORAL_IQ.csv',QueryCopyDir,DatasetNameOnly,DatasetNameOnly);
%     PAA_File = sprintf('%s\\%s\\Outside\\%s_PAA_IQ.csv',QueryCopyDir,DatasetNameOnly,DatasetNameOnly);
%     JACOR_File = sprintf('%s\\%s\\Outside\\%s_JACOR_IQ.csv',QueryCopyDir,DatasetNameOnly,DatasetNameOnly);
%     JACOR_LA_File = sprintf('%s\\%s\\Outside\\%s_JACOR-LA_IQ.csv',QueryCopyDir,DatasetNameOnly,DatasetNameOnly);
%     
%     dlmwrite(CORAL_File,CORAL_Data{2},'delimiter',',','precision',4);
%     dlmwrite(PAA_File,PAA_Data{2},'delimiter',',','precision',4);
%     dlmwrite(JACOR_File,JACOR_Data{2},'delimiter',',','precision',4);
%     dlmwrite(JACOR_LA_File,JACOR_LA_Data{2},'delimiter',',','precision',4);
%   
   % Copy other ops file
    Source = sprintf('%s\\%s\\%s_Out_Other.csv',OutputFileDir,DatasetNameOnly,DatasetNameOnly);
	Dest= sprintf('%s\\%s\\%s_Out_Other.csv',QueryCopyDir,DatasetNameOnly,DatasetNameOnly);
    system(sprintf('copy %s %s',Source,Dest));
     
    fprintf('%d/%d processed \n',k,N);
end



