% This script plots no. of groups produced and offline time construction
% for 84 datasets
clear;clc

%% Initialization
FileAddr = 'groupAllDatasetsFinal.xlsx';

[~,DatasetName] = xlsread(FileAddr,'SizeSorted','A2:A86');
nGroups = xlsread(FileAddr,'SizeSorted','B2:B86');
Time = xlsread(FileAddr,'SizeSorted','C2:C86');

% nGroupsLog=log10(nGroups);
% TimeLog=log10(Time);

Options.YLabelSize = 25;
Options.YAxisSize = 20;
Options.LegendSize = 20;
Options.LineWidth = 3;

%% Output
close all;
MuzPlot(DatasetName,Time,nGroups,1, '',Options );

% [~,TimeIdx]=sort(Time);
% MuzPlot(DatasetName(TimeIdx),TimeLog(TimeIdx),nGroupsLog(TimeIdx),2, 'Time sorted',Options);
% 
% [~,GroupIdx]=sort(nGroups);
% MuzPlot(DatasetName(GroupIdx),TimeLog(GroupIdx),nGroupsLog(GroupIdx),3, 'Group sorted',Options);
