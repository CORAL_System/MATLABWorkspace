function MuzPlot(XData,YData1,YData2,FigureNo, FigureTitle,Options)

Time = YData1;
nGroups = YData2;
DatasetName = XData;
LineWidth = Options.LineWidth;
YLabelSize = Options.YLabelSize;
LegendSize = Options.LegendSize;
YAxisSize = Options.YAxisSize;


h1 = figure(FigureNo);
X = (1:length(nGroups))';
semilogy(X,Time,'k-','LineWidth',LineWidth+1);
ax1 = gca;

ax1.YAxis.FontSize = YAxisSize;
ax1.YTick = [1e-1 1e1 1e3 1e5 1e7];
ax1.YLim = [1e-1 1e6];

% yyaxis left
% ylabel(ax1,{'Offline Time (sec)','No. of clusters'},'FontSize',YLabelSize);
ylabel(ax1,'Offline Time (sec)','FontSize',YLabelSize);
hold on;

%% Right axis
yyaxis right
semilogy(X,nGroups,'k:','LineWidth',LineWidth);
ax1.YAxis(2).Scale = 'log';
ax1.YAxis(2).FontSize = YAxisSize;
ax1.YAxis(2).Color = 'k';
ax1.YAxis(2).TickValues = [1e-1 1e1 1e3 1e5 1e7];
ax1.YAxis(2).Limits = [1e-1 1e6];
ylabel(ax1,'No. of clusters','FontSize',YLabelSize);

legend({'Offline time','No. of clusters'},'Location','southeast','FontSize',LegendSize);
title(FigureTitle);

% Set X-axis
ax1.XTick = X;
ax1.XTickLabel = DatasetName;
ax1.XTickLabelRotation = -90;
ax1.TickLabelInterpreter ='none';
% ax1.LabelFont SizeMultiplier = 1.0;
ax1.XLim = [X(1) X(end)];
% grid on; grid minor;
hold off;


end