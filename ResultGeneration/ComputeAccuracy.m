function Accuracy =ComputeAccuracy(Data,RefData,DataProj,RefDataProj)
% ComputeAccuracy(Data,RefData,DataProj,RefDataProj) computes accuracy with
% respect to the given RefData
% INPUT
% Data:             Input data
% RefData:          Reference data
% DataProj:         Column containing correlation in Data
% RefDataProj:      Column containing correlation in RefData
% OUTPUT
% Accuracy:         Accuracy
%
% Accuracy = \frac{1}{n}\sum_{i=1}^n|y_i-r_i| is the l1 norm between
% correlation vector of given system (y) and correlation vector of
% reference (r)

y = [Data{1}(:,DataProj); 
    Data{2}(:,DataProj)];
r =  [RefData{1}(:,RefDataProj); 
    RefData{2}(:,RefDataProj)];

% Remove nan from data
BadInd = isnan(y) | abs(y)>1;
y(BadInd) = [];
r(BadInd) = [];

y = abs(y); 
r= abs(r);

Accuracy = (1- mean(abs(y-r)))*100;

end
