function    [S1,S2]=GetTopNQuerySamples(Data,RefData,DataProj,RefDataProj,TopN)
% [S1,S2]=GetTopNQuerySamples(Data,RefData,DataProj,RefDataProj) returns
% top query indices which are most accurate w-r-t RefData
% 
% INPUT
% Data:             Input data (e.g CORAL data)
% RefData:          Reference data (e.g. NAIVE data)
% DataProj:         Column containing correlation in Data
% RefDataProj:      Column containing correlation in RefData
% TopN:             Top N queries to be picked up out of all.
%
% OUTPUT
% S1:               Inside data query indices 
% S2:               Outside data query indices 
%
% 
% WARNING: Function may not return top N queries if Data doesn't have
% queries equal to no. of queries in RefData. 

S = cell(size(Data));

for k = 1:2
    s1 = size(Data{k},1);                % no. of queries in data
    s2 = size(RefData{k},1);
    
    DataCorr = Data{k}(:,DataProj);
    RefDataCorr = RefData{k}(1:s1,RefDataProj);
    Error = abs(abs(DataCorr) - abs(RefDataCorr));  % Compute error
    [~,idx] = sort(Error);
    
    m = round(TopN/s2*length(idx));       % Pick 
    
    S{k} = idx(1:m);
end

S1 = S{1};
S2 = S{2};

end