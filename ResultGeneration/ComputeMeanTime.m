function MeanTime = ComputeMeanTime(Data, ProjectionCol)
% function ComputeMeanTime(Data, ProjectionCol) computes mean time from the
% given data
% INPUT
% Data:             Input data
% ProjectionCol:    Column containing time
% OUTPUT
% MeanTime:         Mean time
%
% Data is a cell array with two cells. 1st cell for inside queries data and
% 2nd cell for outside queries data


t1 = [Data{1}(:,ProjectionCol); 
    Data{2}(:,ProjectionCol)];

MeanTime = mean(t1);

end