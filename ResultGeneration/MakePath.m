function OutputDir = MakePath(Input)
% function OutputDir = MakePath(Input) makes file/directory path out of Input
% components
% INPUT
% Input:            cell array of strings
% 
% OUTPUT:
% OutputDir:        file/directory path

OutputDir = Input{1};
for i = 2:length(Input)
   OutputDir = [OutputDir '\' Input{i}];
end

end