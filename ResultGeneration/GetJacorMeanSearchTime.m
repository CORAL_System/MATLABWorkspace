function    MeanSearchTime = GetJacorMeanSearchTime(Data,TimeProj,FFT_TimeProj)
% function    MeanSearchTime = GetJacorMeanSearchTime(Data,TimeProj,FFT_TimeProj)
% computes mean search time taken by JACOR beside the fft time. 
% INPUT
% Data:             Input data
% TimeProj:         Column containing full time taken by JACOR
% FFT_TimeProj:     Column containing FFT time taken by JACOR
% OUTPUT
% MeanSearchTime:   Mean search time
%
% Data is a cell array with two cells. 1st cell for inside queries data and
% 2nd cell for outside queries data
    

% MeanTime = mean([Data{1}(:,ProjectionCol); 
%             Data{2}(:,ProjectionCol)]);

ind = [FFT_TimeProj TimeProj];
        
t1 = [Data{1}(:,ind);
    Data{2}(:,ind)];
t2 = diff(t1,1,2);

MeanSearchTime = mean(t2);

end