% This file plots the generated results
clearvars -except DatasetNameList; clc; close all;
%% User Control

DatasetNameFileAddr = 'C:\Datasets\FileList-SizeSorted2-1.xlsx';

NAIVE_Sys_Name = 'Baseline';
CORAL_Sys_Name = 'CORAL';
PAA_Sys_Name = 'PAA';
JACOR_Sys_Name = 'JACOR';
JACOR_LA_Sys_Name = 'JACOR-LA';
JACOR_Search_Sys_Name = 'JACOR-\nSearch';
JACOR_LA_Search_Sys_Name = 'JACOR-\nLA';


NAIVE_Style = 'r*--';
CORAL_Style = 'k+-';
PAA_Style = 'gv:';
JACOR_Style = 'b^--';
JACOR_LA_Style = 'ms--';

LegendFontSize = 20;
YLabelFontSize = 35;
AxisFontSize = 20;
LineWidth=4;
MarkerSize = 10;
TitleFont = 25;


ResponseTimeText = 'Response Time (sec)';
ResponseTimeText2 = {'Response', 'Time (sec)'};

%% Initialization
load('TimeData_Top50_Run2-22.mat');
load('AccuracyData_Top50_Run2-22.mat');
load('OtherOpData4.mat');

N = length(CORAL_Time);

if (~exist('DatasetNameList','var'))
[~,DatasetNameList] = xlsread(DatasetNameFileAddr,'A2:A86');
end

NewDatasetNameList = cell(size(DatasetNameList));

%% Basic Processing

% Removing extension from file names
for k = 1:length(DatasetNameList)
    NewDatasetNameList{k} = DatasetNameList{k}(1:end-4);
end

% Finding envelope of other systems
T1 = [NAIVE_Time PAA_Time JACOR_Time JACOR_LA_Time];
UpperLim = max(T1,[],2);
LowerLim = min(T1,[],2);

xAxis=[1:N]';

%% Visualization

% Time Figure

hFig1 = figure(1);
hAxis = gca;

semilogy(xAxis,CORAL_Time,CORAL_Style,'LineWidth',LineWidth,'MarkerSize',MarkerSize); hold on;
semilogy(xAxis,NAIVE_Time,NAIVE_Style); hold on;
semilogy(xAxis,PAA_Time,PAA_Style,'LineWidth',LineWidth,'MarkerSize',MarkerSize); hold on;
semilogy(xAxis,JACOR_Time,JACOR_Style); hold on;
semilogy(xAxis,JACOR_LA_Time,JACOR_LA_Style); hold on;

hLeg1 = legend(CORAL_Sys_Name,NAIVE_Sys_Name,PAA_Sys_Name,JACOR_Sys_Name,...
    JACOR_LA_Sys_Name,'Location','NorthWest');
hLeg1.FontSize = LegendFontSize;
X  = [xAxis; xAxis(end:-1:1)];
Y = [UpperLim; LowerLim(end:-1:1)];
% fill(hAxis,X,Y,[0.5 0.5 0]);
hAxis.YScale = 'log' ;

hAxis.YAxis.FontSize = AxisFontSize;
ylabel('Response Time (sec)','FontSize',YLabelFontSize);
hAxis.XTickLabels = NewDatasetNameList;
hAxis.XTickLabelRotation = -90;
hAxis.XRuler.TickLabelInterpreter = 'none';
hAxis.XTick = xAxis;
hAxis.XLim = [1 N];
hAxis.YLim = [1e-4 1e3];
grid on; grid off; grid on; 

hFig1.Position = [0 100 1680 750];

%% Accuracy Plot
hFig2 = figure(2);

plot(xAxis,CORAL_Acc,CORAL_Style,'LineWidth',LineWidth,'MarkerSize',MarkerSize); hold on;
plot(xAxis,PAA_Acc,PAA_Style,'LineWidth',LineWidth,'MarkerSize',MarkerSize); hold on;
plot(xAxis,JACOR_Acc,JACOR_Style,'LineWidth',1); hold on;
plot(xAxis,JACOR_LA_Acc,JACOR_LA_Style,'LineWidth',1); hold off;

hLeg2 = legend(CORAL_Sys_Name,PAA_Sys_Name,JACOR_Sys_Name,...
    JACOR_LA_Sys_Name,'Location','SouthWest');
hLeg2.FontSize = LegendFontSize;
hLeg2.Position([1 2]) = [0.33 0.45];

hAxis2 = gca;
hAxis2.YAxis.FontSize = AxisFontSize;
ylabel(hAxis2,'Accuracy (%)','FontSize',YLabelFontSize);
hAxis2.XTickLabels = NewDatasetNameList;
hAxis2.XTickLabelRotation = -90;
hAxis2.XRuler.TickLabelInterpreter = 'none';
hAxis2.XTick = xAxis;
hAxis2.XLim = [1 N];
hAxis2.YLim = [84 101];
grid on;

hFig2.Position = [0 100 1680 600];
%% JACOR SERCH ONLY TIME FIGURE
% 
% figure(3)
% 
% semilogy(xAxis,CORAL_Time,CORAL_Style); hold on;
% semilogy(xAxis,JACOR_SearchTime,JACOR_Style); hold on;
% semilogy(xAxis,JACOR_LA_SearchTime,JACOR_LA_Style); hold off;
% 
% hLeg3 = legend(CORAL_Sys_Name,JACOR_Sys_Name,...
%     JACOR_LA_Sys_Name,'Location','NorthWest');
% hLeg3.FontSize = LegendFontSize;
% 
% hAxis3 = gca;
% ylabel('Running Time (sec)','FontSize',YLabelFontSize);
% hAxis3.XTickLabels = NewDatasetNameList;
% hAxis3.XTickLabelRotation = -90;
% hAxis3.XTick = xAxis;
% hAxis3.XLim = [1 N];
% grid on;


%% MEAN RESPONSE TIME

w = 1100;

MeanTime = mean([JACOR_SearchTime JACOR_LA_SearchTime CORAL_Time PAA_Time ...
    JACOR_LA_Time JACOR_Time NAIVE_Time ]);


hFig4=figure(4);
hStem = stem(MeanTime);
hStem.LineWidth = 35;
hStem.Marker = 'none';
xlim([0.5 7.5]);
ylim([1e-2 3e2])
Pos = MeanTime;
text(1:length(MeanTime),Pos,num2str(MeanTime','%0.2f'),'vert','bottom',...
        'horiz','center','FontSize',LegendFontSize); 
% box off;

hAxis4= gca;
hAxis4.YScale = 'log';
hAxis4.XTickLabels = {'JACOR-S', 'JACOR-LA-S',CORAL_Sys_Name,  ...
    PAA_Sys_Name, JACOR_LA_Sys_Name, JACOR_Sys_Name, NAIVE_Sys_Name};
hAxis4.FontSize = 16;
hAxis4.YAxis.FontSize = AxisFontSize;
ylabel(hAxis4, ResponseTimeText2,'FontSize',YLabelFontSize);
grid on; grid off; grid on;

hFig4.Position = [0 528 w 420];

%% MEAN OTHER-OPERATION TIME PLOT
hFig5 = figure(5);
CORAL_SC =0.05;
data = [CORAL_GCS, NAIVE_GCS CORAL_SC, NAIVE_SC];
hStem2 = stem(data);
hStem2.LineWidth = 35;
hStem2.Marker = 'none';
xlim([0.5 4.5]);
text(1:length(data),data,num2str(data','%0.2f'),'vert','bottom',...
        'horiz','center','FontSize',LegendFontSize); 
box on;

hAxis5= gca;
hAxis5.YScale = 'log';
hAxis5.XTick = 1:length(data);
hAxis5.XTickLabels = {'GCS-CORAL','GCS-Baseline','SC-CORAL','SC-Baseline' };
hAxis5.FontSize = AxisFontSize;
ylabel(hAxis5, ResponseTimeText2,'FontSize',YLabelFontSize);
grid on; grid off; grid on;

hFig5.Position = [0 528 1680 420];

%% OFFLINE CONSTRUCTION VS ST
hFig6 = figure(6);
TitlePosition = [0.5, -0.4];
AxisPosChange = [0 0.14 0 -0.2];

LineStyle = {'-o','-<','-.v','-->','-+','-^'};


subplot(121);
hAxis61 = gca;
ST_Vec= (0.2:0.2:1)'; 
nClusterData = [1919	256	74	37	24;
                158870	6396	109	61	60;
                88813	1024	175	97	96;
                757560	10967	186	129	128;
                873436	8801	345	153	152;
                16136314	40497	529	513	512]';

OfflineProcTime = [0.092	0.049	0.027	0.023	0.022;
                  39.57     5.62	0.55	0.49	0.4;
                  122.251	1.96	1.14	1.122	1.1;
                  10254.3	203.166	79.663	79.084	76;
                  6124.79	38.863	17.89	17.54	17;
                  44192.5	64.524	50.726	48.683	48]';
              
% No. of Cluster verses ST plot
semilogy(hAxis61,ST_Vec, OfflineProcTime(:,1),LineStyle{1},'LineWidth',LineWidth, 'MarkerSize', MarkerSize); hold on;
semilogy(hAxis61,ST_Vec, OfflineProcTime(:,2),LineStyle{2},'LineWidth',LineWidth, 'MarkerSize', MarkerSize); hold on;              
semilogy(hAxis61,ST_Vec, OfflineProcTime(:,3),LineStyle{3},'LineWidth',LineWidth, 'MarkerSize', MarkerSize,'Color','k'); hold on;              
semilogy(hAxis61,ST_Vec, OfflineProcTime(:,4),LineStyle{4},'LineWidth',LineWidth, 'MarkerSize', MarkerSize); hold on;              
semilogy(hAxis61,ST_Vec, OfflineProcTime(:,5),LineStyle{5},'LineWidth',LineWidth, 'MarkerSize', MarkerSize,'Color','r'); hold on;              
semilogy(hAxis61,ST_Vec, OfflineProcTime(:,6),LineStyle{6},'LineWidth',LineWidth, 'MarkerSize', MarkerSize,'Color','k'); hold off;              
              
hAxis61.XTick = ST_Vec;
hAxis61.YTick = [1e-2 1 1e2 1e4];
hAxis61.FontSize = AxisFontSize;

grid on; grid off; grid on;             
xlabel(hAxis61,'ST','FontSize',YLabelFontSize);              
ylabel(hAxis61,{'Preprocessing', 'Time (sec)'},'FontSize',YLabelFontSize);
ylim(hAxis61,[1e-2 1e5]);


hText61 = title('(a)','FontSize',TitleFont,'FontWeight','normal');

hAxis61.Position = hAxis61.Position + AxisPosChange;
hText61.Units = 'normalized';
hText61.Position = TitlePosition;

% hLeg61 = legend(hAxis61,'ItalyPower','SyntheticControl','ECG',...
%         'TwoPattern','Wafer','EarthQuakes');
% hLeg61.FontSize = LegendFontSize;


% Preprocessing time verses ST plot
subplot(122);
hAxis62 = gca;
semilogy(hAxis62,ST_Vec, nClusterData(:,1),LineStyle{1},'LineWidth',LineWidth, 'MarkerSize', MarkerSize); hold on;
semilogy(hAxis62,ST_Vec, nClusterData(:,2),LineStyle{2},'LineWidth',LineWidth, 'MarkerSize', MarkerSize); hold on;              
semilogy(hAxis62,ST_Vec, nClusterData(:,3),LineStyle{3},'LineWidth',LineWidth, 'MarkerSize', MarkerSize,'Color','k'); hold on;              
semilogy(hAxis62,ST_Vec, nClusterData(:,4),LineStyle{4},'LineWidth',LineWidth, 'MarkerSize', MarkerSize); hold on;              
semilogy(hAxis62,ST_Vec, nClusterData(:,5),LineStyle{5},'LineWidth',LineWidth, 'MarkerSize', MarkerSize,'Color','r'); hold on;              
semilogy(hAxis62,ST_Vec, nClusterData(:,6),LineStyle{6},'LineWidth',LineWidth, 'MarkerSize', MarkerSize,'Color','k'); hold off;              
              
grid on; grid off; grid on;
hAxis62.XTick = ST_Vec;
hAxis62.YTick = [1e2 1e4 1e6];
hAxis62.FontSize = AxisFontSize;

xlabel(hAxis62,'ST','FontSize',YLabelFontSize);              
ylabel(hAxis62,'No. of clusters','FontSize',YLabelFontSize);
ylim(hAxis62,[2e1 3e7]);
    

    
hText62 = title('(b)','FontSize',TitleFont,'FontWeight','normal');
hAxis62.Position = hAxis62.Position + AxisPosChange;
hText62.Units = 'normalized';
hText62.Position = TitlePosition;

[hLeg6] = legend(hAxis61,'ItalyPower','SyntheticControl','ECG',...
        'TwoPattern','Wafer','EarthQuakes');
hLeg6.FontSize = LegendFontSize;
hLeg6.Orientation= 'horizontal';
    
hLeg6.Position(1) = 0.2;    
hLeg6.Position(2) = 0.90;
% 
% hLegLine6 = hIcons6(6+[1:2:11]);
% hLegMark6 = hIcons6(6+[2:2:12]);
% 
% for k =1:6
% hLegLine6(k).YData = [0.9091 0.9091];
% hLegMark6(k).YData = [0.9091];
% end

hFig6.Position = [0 400 1680 530];


%% ST TRADEOFF PLOTS
hFig7 = figure(7); 
ST = (0.2:0.2:0.8)';
MeanError = [0.016 0.0262 0.06 0.001]';
MeanResponseTime = [0.03 0.00492 0.01158 0.0735]';

hAxis7 = gca;
plot(ST,MeanError,'ko-','LineWidth',LineWidth, 'MarkerSize', MarkerSize ); hold on;
hAxis7.FontSize = AxisFontSize;
xlabel('ST','FontSize',YLabelFontSize);
ylabel('Error','FontSize',YLabelFontSize);
hAxis7.YAxis(1).Label.FontSize = YLabelFontSize;
% hAxis7.YAxis(1).TickValues = 0:0.2:0.8;
% hAxis7.YAxis(1).TickLabels = {'0', '0.2', '0.4', '0.6', '0.8'};
ylim(hAxis7,[0 0.08]);

yyaxis right;
plot(ST,MeanResponseTime,'k^:','LineWidth',LineWidth, 'MarkerSize', MarkerSize ); hold off;
ylabel({'Response', 'Time (sec)'},'FontSize',YLabelFontSize,'Color','k');
hAxis7.YAxis(2).Color = [0 0 0];
% hAxis7.YAxis(2).TickValues = 0:0.2:0.6;


hAxis7.XTick = ST;
xlim(hAxis7,[0.2, 0.8]);

hLgd7 = legend('Error', 'ResponseTime','Location','NorthWest');
hLeg7.FontSize = LegendFontSize; 

grid on; grid off; grid on;

hFig7.Position = [0 528 1680 420];


%% MEAN Accuracy PLOT
hFig8 = figure(8);

MeanAcc = mean([PAA_Acc CORAL_Acc JACOR_Acc JACOR_LA_Acc]);


hStem8 = stem(MeanAcc);
hStem8.LineWidth = 35;
hStem8.Marker = 'none';
xlim([0.5 4.5]);
% ylim([1e-2 3e2])
Pos = MeanAcc;
text(1:length(MeanAcc),Pos,num2str(MeanAcc','%0.2f'),'vert','bottom',...
        'horiz','center','FontSize',LegendFontSize); 


hAxis8= gca;
hAxis8.XTick = [1 2 3 4];
hAxis8.XTickLabels = {PAA_Sys_Name,CORAL_Sys_Name, JACOR_Sys_Name, JACOR_LA_Sys_Name};
hAxis8.FontSize = 16;
hAxis8.YAxis.FontSize = AxisFontSize;
ylabel(hAxis8, 'Accuracy (%)','FontSize',YLabelFontSize);
ylim(hAxis8,[96 100]);
grid on; grid off; grid on;
hFig8.Position = [w 528 1680-w 420];

