% This scrip generates another sequence whose sub sequence are negatively
% and positively correlated by a subsequence in data
clear;clc; close all;

%%
a=0.2;
l1 = 1000;
ind1 = 150:551;
b=0.15;
l2 = 300;

load PlottingDataOnly.mat;

% Following code doesn't run here
% TS1 = data1;
% TS2 = data2;
% 
% 
% SS1 = TS1(ind1);
% 
% SS2 = medfilt1(SS1 + rand(size(SS1))*a*max(SS1),3);
% TS2(ind1+l1) = SS2 - (SS1(1)-TS2(l1));
% 
% SS21 = medfilt1((-1*(SS1-mean(SS1))+mean(SS1)) + rand(size(SS1))*b*max(SS1),3);
% TS2(ind1+l2) = SS21 - (SS21(1)-TS2(l2));
% d = TS2(ind1(end)+l2+1)-TS2(ind1(end)+l2);
% TS2(1:ind1(end)+l2) = TS2(1:ind1(end)+l2)+d;
% 
% TS1(end-99:end)=[];
% TS2(end-99:end)=[];
% 
% R1 = corrcoef(TS1,TS2)
% R2 = corrcoef(SS1,SS2)
% R3 = corrcoef(SS1,SS21)

%%
figure(1);
% plot([TS1,TS2],'LineWidth',1.5);
plot(TS1,'.-','LineWidth',1.5,'Color',[153 153 255]/255); hold on;
plot(TS2,'.-','LineWidth',1.5,'Color',[0 0 102]/255); hold off;

hA1 = gca;
hA1.FontSize=25;
hA1.XTick = 0:200:1600;
ylim([0 max(TS1)]);
xlim([0 1600]);
% legend('TS1','TS2','Location','SouthWest','Orientation','horizontal');legend('boxoff');
% grid on; grid minor;
% hold on;
xlabel('time');
title('TS1 and TS2');
% ylabel('Magnitude');
% annotation('doublearrow',[0.205 .3],[.8 0.8])
%%
figure(2);
subplot(211); 
% plot([SS1,SS2],'LineWidth',1.5,'LineStyle','-');
plot(SS1,'LineWidth',1.5,'LineStyle','-','Color',[153 153 255]/255); hold on;
plot(SS2,'LineWidth',1.5,'LineStyle','-','Color',[0 0 102]/255); hold off;

xlim([1 402]); ylim([50 105]); 
% grid minor;  
title('\bf{A and B}','Interpreter','latex'); 
% legend('A','B','Location','best','Orientation','horizontal'); legend('boxoff');
xlabel('time');
hA21 = gca; hA21.FontSize=25;
subplot(212); 
% plot([SS1 SS21],'LineWidth',1.5); xlim([1 402]); ylim([50 105]);
plot(SS1,'LineWidth',1.5,'LineStyle','-','Color',[153 153 255]/255); hold on;
plot(SS21,'LineWidth',1.5,'LineStyle','-','Color',[0 0 102]/255); hold off;
% grid minor; 
xlabel('time');
xlim([1 402]); ylim([50 105]); 
title('\bf{A and C}','Interpreter','latex'); 
% legend('A','C','Location','best','Orientation','horizontal'); legend('boxoff');
hA22 = gca; hA22.FontSize=25;
hA22.Position = hA22.Position.*[1 1 1 1]

% subplot(313); plot(SS21,'LineWidth',1.5); ylim([0 110]);grid minor;
% plot([SS1 SS2],'LineWidth',1.5);
% legend('a','b'); ylim([0 110]);grid minor;


% save CompleteData.mat 

