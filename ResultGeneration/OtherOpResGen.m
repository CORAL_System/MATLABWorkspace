% This script generates results for OtherOp (groups of correlated seq and
% self corr)
clear;clc
%% USER CONTROL
InputDataDir = 'C:\CORAL_System\MATLABWorkspace\ResultGeneration\OtherOpData';
OutputFileSuffix = '_Out_Other.csv';
DataRangeSelect = 'A2:H11';

OutputFile = 'OtherOpData4.mat';

%% INITIALIZATION
Filelist = dir([InputDataDir '\' '*' OutputFileSuffix]);
N = length(Filelist);

Acc = zeros(1,8);
TotalQueries = 0;

%% ALGORITHM
for k = 1:N
    
	% Prepare Dataset names
	DatasetName = Filelist(k).name;                   % Dataset name with extension
    DatasetNameOnly = DatasetName(1:end-4);      % Dataset name without extension
    DatasetExt = DatasetName(end-2:end);
	
    FileAddr = sprintf('%s\\%s%s',InputDataDir,DatasetName);
    data = xlsread(FileAddr,DataRangeSelect);
    nQueries = size(data,1);
    
    Acc = Acc + sum(data);
    TotalQueries = TotalQueries + nQueries;
    
end

% Add time for 8 other datasets (OtherOpData2.mat)
% WARNING: wrong results( mean query time was multiplied by 100 instead of
% 10)
% Acc = Acc + [0 0 0 13334311.95 288.83 0 71.6 4.231];
% TotalQueries = TotalQueries + 10*8;

% Add time for 7 other datasets (OtherOpData21.mat)- 
% WARNING: wrong results( mean query time was multiplied by 100 instead of
% 10)
% Acc = Acc + ;
% TotalQueries = TotalQueries + 10*7;

% Add time for 5 other datasets (OtherOpData22.mat)
Acc = Acc + [0 0 0 43260.201 10.525 0 27.898 0.744];
TotalQueries = TotalQueries + 10*5;


% Computing mean now
MeanData = Acc/TotalQueries;


NAIVE_GCS = MeanData(4);
CORAL_GCS = MeanData(5);
NAIVE_SC = MeanData(7);
CORAL_SC = MeanData(8);


%% RESULT

save(OutputFile,'*_GCS', '*_SC');