% This script makes outside queries 
clear;clc

%% User Control
InputDatasetAddr = 'C:\CORAL_System\MATLABWorkspace\PrepDS4OutsideQueries\Lighting2.txt';
OutsideQueryDir = 'C:\CORAL_System\MATLABWorkspace\PrepDS4OutsideQueries\Outside';
NumOfQueries = 20;
alpha = 0.1;            % fraction of dataset to be used for outside query generations


%% INITIALIZATION
OutputDatasetAddr = [InputDatasetAddr(1:end-4) '_Outside' InputDatasetAddr(end-3:end)];
DL = ' ';               % delimiter to be used

%% ALGORITHM
Data = dlmread(InputDatasetAddr);
timeSeriesN = size(Data,1);
timeSeriesL = size(Data,2);
p = round(timeSeriesN *alpha);          % no. of rows to be separated for outside query generation
DataA = Data(1:p,:);
DataB = Data(p+1:end,:);

for k=1:NumOfQueries
TSId = RandMuz(1, p);
Len = RandMuz(2,timeSeriesL-1);
Offset = RandMuz(1,timeSeriesL - Len+1);

PickedQuery = DataA(TSId,Offset:Offset+Len-1);

OutsideQueryAddr = sprintf('%s\\OutsideQuery%d.txt',OutsideQueryDir,k);
[fID,Err] = fopen(OutsideQueryAddr,'w');
fprintf(fID,'%f ', PickedQuery(1:end-1));
fprintf(fID,'%f', PickedQuery(end));
fclose(fID);

% dlmwrite(OutsideQueryAddr,PickedQuery,'delimiter',DL,'newline','unix');


end

dlmwrite(OutputDatasetAddr,DataB,DL);


%% VISUALIZATION
msgbox(sprintf('%d outside queries have been generated',NumOfQueries));


%% SUPPORT FUNCTION
function output = RandMuz(Start,End)
m = End-Start;
c = Start;

output = round(m*rand+c);

end

