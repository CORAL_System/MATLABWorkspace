% This script makes outside queries 
clear;clc

%% User Control

InputDatasetDir = 'C:\CORAL_System\MATLABWorkspace\PrepDS4OutsideQueriesBatch\UCRDatasets';
OutputDir = 'C:\CORAL_System\MATLABWorkspace\PrepDS4OutsideQueriesBatch\Output';
OutputFileName = 'FileList.xlsx';
NumOfQueries = 80;
alpha = 0.1;            % fraction of dataset to be used for outside query generations


%% INITIALIZATION

FileList = dir([InputDatasetDir '\*.txt']);
N = length(FileList);

FileNames = cell(N,1);
timeSeriesN = zeros(N,1);
timeSeriesL = zeros(N,1);
OutsideDataN = zeros(N,1);


mkdir(OutputDir);
DL = ' ';               % delimiter to be used
OutputFileAddr = [OutputDir '\' OutputFileName];


for k = 1:N
    
    CurrentFileName = FileList(k).name;
    CurrentFileNameOnly = FileList(k).name(1:end-4);
    CurrentFileExt = FileList(k).name(end-3:end);
    
    InputDatasetAddr = [InputDatasetDir '\' CurrentFileName]; 
    OusideDatasetAddr = [OutputDir '\' CurrentFileNameOnly '_Outside' CurrentFileExt];
    OutsideQueryDir = [OutputDir '\' CurrentFileNameOnly];
  
    mkdir(OutsideQueryDir);
    
    %% ALGORITHM
    Data = dlmread(InputDatasetAddr);
    OutsideData = MakeOutsideQueries(Data,OutsideQueryDir,NumOfQueries,alpha);
    
    dlmwrite(OusideDatasetAddr,OutsideData,DL);
    
    FileNames{k} = CurrentFileName;
    timeSeriesN(k) = size(Data,1);
    timeSeriesL(k) = size(Data,2);
    OutsideDataN(k) = size(OutsideData,1);
end

%% OUTPUT

T = table(FileNames,timeSeriesN,timeSeriesL,OutsideDataN);
writetable(T,OutputFileAddr);


