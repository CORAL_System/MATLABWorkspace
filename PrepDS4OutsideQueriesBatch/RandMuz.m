function output = RandMuz(Start,End)
m = End-Start;
c = Start;

output = round(m*rand+c);

end