function DataOutside=MakeOutsideQueries(Data,OutsideQueryDir,NumOfQueries,alpha)

timeSeriesN = size(Data,1);
timeSeriesL = size(Data,2);
p = round(timeSeriesN *alpha);          % no. of rows to be separated for outside query generation
DataA = Data(1:p,:);
DataB = Data(p+1:end,:);

for k=1:NumOfQueries
TSId = RandMuz(1, p);
Len = RandMuz(2,timeSeriesL-1);
Offset = RandMuz(1,timeSeriesL - Len+1);

PickedQuery = DataA(TSId,Offset:Offset+Len-1);

OutsideQueryAddr = sprintf('%s\\OutsideQuery%d.txt',OutsideQueryDir,k);
[fID,Err] = fopen(OutsideQueryAddr,'w');
fprintf(fID,'%f ', PickedQuery(1:end-1));
fprintf(fID,'%f', PickedQuery(end));
fclose(fID);

% dlmwrite(OutsideQueryAddr,PickedQuery,'delimiter',DL,'newline','unix');


end

DataOutside = DataB;


end