clear;clc;
%% User Control
EXE_Addr = 'CORAL2_OtherOp2-2.exe';

DatasetListFileAddr = 'FileList-SizeSorted2-OtherOps.xlsx';
DatasetListFileRange = 'A2:H86';

InputDatasetDir = 'C:\Datasets\UCRDatasets';
OutputDir = 'C:\Datasets\Run1';

OutputFileSuffix = '_Out_Other';
OutputFileExt = 'csv';

%% Initialization
Mode = 11;
CommandFileAddr = 'none';

[Data,DatasetNameList] = xlsread(DatasetListFileAddr,DatasetListFileRange);
LVec = Data(:,2);
NVec = Data(:,1);               % This is N for outside data
ST_Vec = Data(:,5);
nRunsVec = Data(:,6);
ProcessDS_Vec = Data(:,7);

M = length(DatasetNameList);        % no. of datasets

%% Algorithm
for k=1:M
    
    if ProcessDS_Vec(k)==1
    DatasetName = DatasetNameList{k};                   % Dataset name with extension
    DatasetNameOnly = DatasetNameList{k}(1:end-4);      % Dataset name without extension
    DatasetExt = DatasetNameList{k}(end-2:end);

%     fprintf('%d:Processing %s\n',k,DatasetNameOnly);
    
        
        N = NVec(k);
        L = LVec(k);
        ST = ST_Vec(k);
        nRuns = nRunsVec(k);
        InputDatasetAddr = [InputDatasetDir '\' DatasetName];
        OutputFileName = [DatasetNameOnly OutputFileSuffix '.' OutputFileExt];
        OutputFilePath = [OutputDir '\' DatasetNameOnly '\' OutputFileName];
        CMD_Str = sprintf('%s %d %d %d %f %s %s %d %s',EXE_Addr,Mode,N,L,ST,InputDatasetAddr,OutputFilePath,nRuns,CommandFileAddr);
        fprintf('%s\n',CMD_Str);
        %         Status = system(CMD_Str);
    end
end
%% Output