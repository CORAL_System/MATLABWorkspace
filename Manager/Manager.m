% This script runs CORAL and other systems on multiple datasets in Batch
clear; clc

%% USER CONTROL
DatasetListFileAddr= 'C:\Datasets\FileList-SizeSorted2-1.xlsx';
DatasetListFileRange = 'A2:J86';

InputDatasetDir = 'C:\Datasets\UCRDatasets';
OutputDir = 'C:\Datasets\Run1';

CORAL_EXE_Addr = 'C:\CORAL_System\MATLABWorkspace\Manager\CORAL2-2.exe';
JACOR_EXE_Addr = 'C:\CORAL_System\MATLABWorkspace\Manager\JACOR_Batch.exe';
PAA_EXE_Addr = 'C:\CORAL_System\MATLABWorkspace\Manager\PAA_Batch.exe';

QueryType = 1;                  % 1=inside,  2= outside
QueryStartIdx = 39;
QueryEndIdx = 79;


JACOR_Algo = 1;

%% INITIALIZATION
[Data,DatasetNameList] = xlsread(DatasetListFileAddr,DatasetListFileRange);
LVec = Data(:,2);
NVec = Data(:,3);               % This is N for outside data
ST_Vec = Data(:,5);
doCORAL_Vec = Data(:,6);
doJACOR_Vec = Data(:,7);
doPAA_Vec = Data(:,8);
ProcessDS_Vec = Data(:,9);
M = length(DatasetNameList);        % no. of datasets


OD_Suffix = '_Outside';                         % outside dataset file suffix
IQ_CommandFileSuffix = '_CommInQ';
OQ_CommandFileSuffix = '_CommOutQ';
CommandFileSuffix = {IQ_CommandFileSuffix,OQ_CommandFileSuffix};
CommandFileExt = 'txt';
Mode=10;

QueryTypeStr = {'Inside','Outside'};
QueryNameTmpl = {'InsideQuery','OutsideQuery'};

% PAA Params
PAA_OutputFileDelim = ',';
PAA_OutputFilePrefix = {'_PAA_IQ.csv','_PAA_OQ.csv'};

% JACOR Params
JACOR_OutputFileDelim = ',';
JACOR_OutputFilePrefix = {'_JACOR_IQ.csv', '_JACOR-LA_IQ.csv';
                        '_JACOR_OQ.csv', '_JACOR-LA_OQ.csv'};

JACOR_StepSize = 1;
JACOR_MaxComp = 1;
JACOR_K = 1;

%% ALGORITHM

% Loop over all the datasets
for k =1:M
    if ProcessDS_Vec(k)==1                              % only run for selected datasets
    DatasetName = DatasetNameList{k};                   % Dataset name with extension
    DatasetNameOnly = DatasetNameList{k}(1:end-4);      % Dataset name without extension
    DatasetExt = DatasetNameList{k}(end-2:end);

    fprintf('%d:Processing %s\n',k,DatasetNameOnly);
    
    % Process CORAL    
    if doCORAL_Vec(k)==1

        N = NVec(k);
        L = LVec(k);
        ST = ST_Vec(k);
        InputDatasetAddr = [InputDatasetDir '\' DatasetNameOnly OD_Suffix '.' DatasetExt];
        OutputFilePath = 'none';
        nRuns = 0;
        CommandFileAddr = [OutputDir '\' DatasetNameOnly CommandFileSuffix{QueryType} '.' CommandFileExt];
        
        CMD_Str = sprintf('%s %d %d %d %f %s %s %d %s',CORAL_EXE_Addr, Mode,N,L,ST,InputDatasetAddr,OutputFilePath,nRuns,CommandFileAddr);
        status = system(CMD_Str);
    end
    
    % Process JACOR
    if doJACOR_Vec(k)==1          
        N = NVec(k);
        L = LVec(k);
        InputDatasetAddr = [InputDatasetDir '\' DatasetNameOnly OD_Suffix '.' DatasetExt];
        QueryTemplate = [OutputDir '\' DatasetNameOnly '\' QueryTypeStr{QueryType} '\' QueryNameTmpl{QueryType}];
        JACOR_OutputFilePath = [OutputDir '\' DatasetNameOnly '\' QueryTypeStr{QueryType} '\' DatasetNameOnly JACOR_OutputFilePrefix{QueryType,JACOR_Algo+1}];
        
        CMD_StrJACOR = sprintf('%s %s %s %d %d %s %s %d %d %d %d %d',JACOR_EXE_Addr, InputDatasetAddr, QueryTemplate,QueryStartIdx, QueryEndIdx ,...
            JACOR_OutputFileDelim,JACOR_OutputFilePath, N*L, JACOR_StepSize,JACOR_Algo, JACOR_MaxComp,JACOR_K);
        statusJACOR = system(CMD_StrJACOR);
    end
    
    % Process PAA
    if doPAA_Vec(k)==1
        InputDatasetAddr = [InputDatasetDir '\' DatasetNameOnly OD_Suffix '.' DatasetExt];
        QueryTemplate = [OutputDir '\' DatasetNameOnly '\' QueryTypeStr{QueryType} '\' QueryNameTmpl{QueryType}];
        PAA_OutputFilePath = [OutputDir '\' DatasetNameOnly '\' QueryTypeStr{QueryType} '\' DatasetNameOnly PAA_OutputFilePrefix{QueryType}];
        
        CMD_StrPAA = sprintf('%s %s %s %d %d %s %s',PAA_EXE_Addr, InputDatasetAddr, QueryTemplate,QueryStartIdx, QueryEndIdx , PAA_OutputFileDelim,PAA_OutputFilePath);
        StatusPAA = system(CMD_StrPAA);
    end
    

    end   
end

%% OUTPUT