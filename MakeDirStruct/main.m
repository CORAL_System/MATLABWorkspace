clear;clc

%% USER CONTROL
DatasetListFileAddr= 'C:\Datasets\FileList-SizeSorted2-1.xlsx';
DatasetListFileRange = 'A2:A86';
OutputDir = 'C:\Datasets\Run1';
OQ_Source = 'C:\CORAL_System\MATLABWorkspace\PrepDS4OutsideQueriesBatch\Output';

MakeDirFlag = true;
Copy_OQ_Flag = true;

%% INITIALIZATION
[~,DatasetNameList] = xlsread(DatasetListFileAddr,DatasetListFileRange);


M = length(DatasetNameList);

%% ALGORITHM
for k = 1:M
    DatasetName = DatasetNameList{k}(1:end-4);
    
    % Make directories
    if (MakeDirFlag)
        mkdir([OutputDir '\' DatasetName]);
        mkdir([OutputDir '\' DatasetName '\Inside']);
        mkdir([OutputDir '\' DatasetName '\Outside']);
    end
    
    % Copy outside queries
    
    if (Copy_OQ_Flag)
    SrcFile = [OQ_Source '\' DatasetName '\OutsideQuery*.txt'];
    DstAddr = [OutputDir '\' DatasetName '\Outside\'];
    copyfile(SrcFile,DstAddr);
    end
    
    fprintf('%d - %s\n',k,DatasetName);
end


%% OUTPUT