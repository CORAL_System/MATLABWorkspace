import os

import numpy as np
import matplotlib.pyplot as plt

# user control 
datasetDir= '/home/muzammil/WPI/CORAL/datasets'
datasetName = 'Beef.txt'
queryAddr = '/home/muzammil/WPI/CORAL/coral-additional/test-out/Beef/Outside/OutsideQuery75.txt'
ts_id_list = [0, 9, 10]
ts_start_list = [45, 1, 1]
ss_label_list = ['CORAL', 'naive', 'PAA']
# queryLen = 303
# 0	45
# 9	1


# initialization
datasetAddr = os.path.join(datasetDir,datasetName)
assert len(ts_id_list) == len(ts_start_list), "length of ts_id and ts_start lists should be equal"
assert len(ts_id_list) == len(ss_label_list), "length of ts_id and subsequence label lists should be equal"


# read data
print('loading dataset: {}'.format(datasetAddr))
data = np.loadtxt(datasetAddr, delimiter=',')
print('data shape: {}'.format(data.shape))


# plot query
query = np.loadtxt(queryAddr, delimiter=' ')
queryLen = len(query)
x = list(range(queryLen))
plt.plot(x, query, label='query')
print('query len: {}'.format(queryLen))

# loop over each subsequnce and process it

for i in range(len(ts_id_list)):
	ts_id = ts_id_list[i]
	ts_start = ts_start_list[i]
	ss_label = ss_label_list[i]

	# get query
	sub_seq = data[ts_id, ts_start:ts_start+queryLen]
	
	# plot the query
	# print('length of {} is : {}'.format(ss_label, sub_seq.shape))


	plt.plot(x, sub_seq, label=ss_label)

plt.xlabel('time')
plt.ylabel('values')
plt.title('Negative correlation illustration')

plt.legend()
plt.show()