function OutputFlag = MakeCommandFile(CommandStruct, CommandFileAddr)
% OutputFlag = MakeCommandFile(CommandStruct, CommandFileAddr) takes in
% CommandStruct and CommandFileAddr to write the command file for CORAL
% system. 
%
% INPUT:
% CommandStruct:            Structure containing all command params
% CommandFileAddr           Address of command file to write on disk
%
% OUTPUT:
% OutputFlag:               true if success


Fields = fieldnames(CommandStruct);

try
    fID = fopen(CommandFileAddr,'w');
    
%     for k=1:length(Fields)
%         fprintf(fID,'%s\n',eval(sprintf('CommandStruct.%s',Fields{k})));
%     end

    fprintf(fID,'%s\n',CommandStruct.InputDatasetAddr);
    fprintf(fID,'%s\n',CommandStruct.OutputFileAddr);
    fprintf(fID,'%s\n',CommandStruct.OutputDirAddr);
    fprintf(fID,'%d\n',CommandStruct.MinLenInsideQuery);
    fprintf(fID,'%d\n',CommandStruct.f_QueryType);
    fprintf(fID,'%d\n',CommandStruct.NumQueryProcess);
    fprintf(fID,'%d\n',CommandStruct.doCORALProcess);
    fprintf(fID,'%d\n',CommandStruct.doNAIVEProcess);
    fprintf(fID,'%f\n',CommandStruct.ST);
    fprintf(fID,'%d\n',CommandStruct.Choice);
    fprintf(fID,'%d\n',CommandStruct.Delta);
    fprintf(fID,'%s\n',CommandStruct.OutQueryFileAddrTmp);
    fprintf(fID,'%d\n',CommandStruct.OutQueryFileStartIdx);
    fprintf(fID,'%d\n',CommandStruct.OutQueryFileEndIdx);
    fprintf(fID,'%s\n',CommandStruct.OutQueryFileExt);

    fclose(fID);
    
    OutputFlag = true;
catch
    OutputFlag = false;
end

end