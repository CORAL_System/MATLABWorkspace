clear;clc

%% USER CONTROL
DatasetListFileAddr= 'C:\Datasets\FileList-SizeSorted2-1.xlsx';
DatasetListFileRange = 'A2:F86';
OutputDir = 'C:\CORAL_System\MATLABWorkspace\MakeCommandFile\Output';


IQ_CommandFileSuffix = '_CommInQ';
OQ_CommandFileSuffix = '_CommOutQ';
CommandFileExt = 'txt';


InputDatasetDir = 'C:\Datasets\UCRDatasets';
OD_Suffix = '_Outside';                         % outside dataset file suffix
CORAL_OutputFileDirPrefix = 'C:\Datasets\Run1';
MinLenInsideQuery = 15;
NumQueryProcess = 80;
doCORALProcess = 1;
doNAIVEProcess = 1;
Choice = 0;
Delta = 100;
OutQueryFileStartIdx = 1;
OutQueryFileEndIdx = 80;
OutQueryFileExt = 'txt';
%% INITIALIZATION
[Data, DatasetNameList] = xlsread(DatasetListFileAddr,DatasetListFileRange);

LVec = Data(:,2);
NVec = Data(:,3);               % This is N for outside data
STVec = Data(:,5);

M = length(DatasetNameList);

%% ALGORITHM
for k = 1:M
    DatasetName = DatasetNameList{k};                   % Dataset name with extension
    DatasetNameOnly = DatasetNameList{k}(1:end-4);      % Dataset name without extension
    DatasetExt = DatasetNameList{k}(end-2:end);
    
    
    % Make command struct
    CommandStruct.InputDatasetAddr = [InputDatasetDir '\' DatasetNameOnly OD_Suffix '.' DatasetExt];
    CommandStruct.OutputFileAddr = [CORAL_OutputFileDirPrefix '\' DatasetNameOnly '\' 'Inside' '\' DatasetNameOnly '_CORAL_IQ.csv'];
    CommandStruct.OutputDirAddr = [CORAL_OutputFileDirPrefix '\' DatasetNameOnly '\' 'Inside'];
    CommandStruct.MinLenInsideQuery = MinLenInsideQuery;
    CommandStruct.f_QueryType = 1;
    CommandStruct.NumQueryProcess = NumQueryProcess;
    CommandStruct.doCORALProcess = doCORALProcess;
    CommandStruct.doNAIVEProcess = doNAIVEProcess;
    CommandStruct.ST = STVec(k);
    CommandStruct.Choice = Choice;
    CommandStruct.Delta = Delta;
    CommandStruct.OutQueryFileAddrTmp = [CORAL_OutputFileDirPrefix '\' DatasetNameOnly '\' 'Outside\OutsideQuery'];
    CommandStruct.OutQueryFileStartIdx = OutQueryFileStartIdx;
    CommandStruct.OutQueryFileEndIdx = OutQueryFileEndIdx;
    CommandStruct.OutQueryFileExt = OutQueryFileExt;
    
    % Inside query command file generation
    CommandFileAddr = [OutputDir '\' DatasetNameOnly IQ_CommandFileSuffix '.' CommandFileExt];
    MakeCommandFile(CommandStruct, CommandFileAddr);

    % Updating params for outside query command file
    CommandStruct.OutputFileAddr = [CORAL_OutputFileDirPrefix '\' DatasetNameOnly '\' 'Outside' '\' DatasetNameOnly '_CORAL_OQ.csv'];
    CommandStruct.f_QueryType = 2;
    
    % Outside query command file generation     
    CommandFileAddr = [OutputDir '\' DatasetNameOnly OQ_CommandFileSuffix '.' CommandFileExt];
    MakeCommandFile(CommandStruct, CommandFileAddr);
    
end


%% OUTPUT